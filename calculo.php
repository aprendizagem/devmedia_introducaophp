<?php
if($_POST){

$distancia  = $_POST['distancia'];
$autonomia  = $_POST['autonomia'];
	if(is_numeric($distancia) && is_numeric($autonomia)){
		if($distancia > 0 && $autonomia >0){
			$valorGasolina = 4.80;
			$valorAlcool   = 3.80;
			$valorDiesel   = 3.90;

			$calculoGasolina = ($distancia / $autonomia ) * $valorGasolina;
			$calculoGasolina  = number_format($calculoGasolina,2,',','.');
			
			$calculoAlcool 	= ($distancia / $autonomia ) * $valorAlcool;
			$calculoAlcool  = number_format($calculoAlcool,2,',','.');

			$calculoDiesel 	= ($distancia / $autonomia ) * $valorDiesel;
			$calculoDiesel  = number_format($calculoDiesel,2,',','.');

			$mensagem.= "<div class='sucesso'>";
			$mensagem.= "O valor total gasto será de:";
			$mensagem.= "<ul>";
			$mensagem.= "<li>Gasolina: R$".$calculoGasolina."</li>";
			$mensagem.= "<li>Álcool: R$".$calculoAlcool."</li>";
			$mensagem.= "<li>Diesel: R$".$calculoDiesel."</li>";
			$mensagem.= "</ul>";
			$mensagem.= "</div>";
	


		}else{ 	$mensagem.= "<div class='erro'";
				$mensagem.= "<b>O valor da distância e autonomia deve maior que zero !<b>";
				$mensagem.= "</div>";
			}
	}else{		$mensagem.= "<div class='erro'";
				$mensagem.= "<b>O valor recebido não é númerico<b>";
				$mensagem.= "</div>";
		}
}else{			$mensagem.= "<div class='erro'";
				$mensagem.= "<b>Não foi informado nenhum dado pelo formulário<b>";
				$mensagem.= "</div>";}
?>

 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Modificar Aqui">
<meta name="keywords" content="HTML,CSS,XML,JavaScript">
<meta name="author" content="Pablo Weslly">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Resultado</title>
</head>

<body>
	<main>
		<div class="painel">
			<h2> Resultado do  cálculo de consumo</h2>
			<div class="conteudo-painel">
				<?php
				echo $mensagem;
				?>
				<a class="botao" href="index.php">Volta</a>
			</div>
		</div>
	</main>
</body>

</html> 