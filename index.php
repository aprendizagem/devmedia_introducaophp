 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Modificar Aqui">
<meta name="keywords" content="HTML,CSS,XML,JavaScript">
<meta name="author" content="Pablo Weslly">
<meta http-equiv="refresh" content="10">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Calculo de Consumo de Combustível</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<main>
		<div class="painel">
				<h2>Instruções</h2>
				<div class="conteudo-painel">
					<p> Esta aplicação tem como finalidade demonstrar os valores que serão gasto com combustível durante uma viagem, com base no consudo do seu veículo,e com a distância determinada por você ! </p>
					<p> Os combustíveis disponíveis para esta cálculo são:</p>
					<ul>
						<li><b>Àlcool</b></li>
						<li><b>Díesel</b></li>
						<li><b>Gasolina</b></li>
					</ul>
				</div>
		</div>

		<div class="painel">
				<h2>Cálculo do valor (R$) do consumo</h2>
				<div class="conteudo-painel">
						<form action="calculo.php" method="POST">
							<label for="distancia"> Distância em quilômetros a ser percorrida</label>
							<input type="number" class="campoTexto" name="distancia" required/>

							<label for=autonomia">Consumo de combustível do veículo (Km/l)</label>
							<input type="number" class="campoTexto" name="autonomia" required />

							<button class="botao" type="submit">Calcular</button>
						</form>
					</div>
		</div>
	</main>
</body>

</html> 